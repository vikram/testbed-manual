#lang scribble/manual

@(require "defs.rkt")

@title[#:tag "powder-tutorial-srs" #:version apt-version]{@(tb) basic srsLTE Tutorial}

This tutorial will walk you through the process of creating a single node
experiment running srsLTE software for simulated UE/eNodeB interaction
(i.e., without SDR hardware). Your experiment will run on a bare-metal machine
that is dedicated for your use for the duration of your experiment. You will
have complete administrative access to this machines, meaning that you have
full ability to customize and modify the srsLTE code, or install other
software as needed.

@section{Objectives}

In the process of taking this tutorial, you will learn to:

@itemlist[
    @item{Log in to @(tb)}
    @item{Create a very basic experiment by using a pre-defined profile}
    @item{Access resources in the experiment that you create}
    @item{Experiment with the tools provided by srsLTE}
    @item{Clean up your experiment when finished}
    @item{Learn where to get more information}
]

@section{Prerequisites}

This tutorial assumes that you have an existing account on @(tb) (Instructions for getting
an account can be found @seclink["register"]{here}.)

@section[#:tag "powder-login-body"]{Logging In}

The first step is to log in to @(tb); @(tb) is available to
researchers and educators who work in radio networking and have accepted the @(tb) AUP.
If you have
an account at one of its federated facilities, like
@link["https://www.emulab.net"]{Emulab} or
@link["http://cloudlab.us"]{CloudLab}, then you already have an account at
@(tb).
@screenshot["powder-front-page.png"]

@section[#:tag "powder-tutorial-body"]{Creating a simple srsLTE experiment}

Once you have logged in to @(tb), you will ``instantiate'' a @seclink["profiles"]{``profile''}
to create an @seclink["experiments"]{experiment}. Profiles are @(tb)'s way of packaging up
configurations and experiments
so that they can be shared with others. Each experiment is separate:
the experiment that you create for this tutorial will be an instance of a profile provided by
the facility, but running on resources that are dedicated to you, which you
have complete control over. This profile uses local disk space on the nodes, so
anything you store there will be lost when the experiment terminates.

@margin-note{The experiment you will instantiate in this tutorial is very small, but @(tb)
will have city-scale infrastructure that can be used for larger-scale
experiments.}

For this tutorial, we will use a basic profile that brings up a single
compute node with srsLTE software pre-installed. As such there is no
actual RF involved, rather, tools provided by srsLTE will be used to
perform "emulated" interaction between a UE and eNodeB. The @(tb) staff have
built this profile by capturing @seclink["disk-images"]{disk images} of an
srsLTE install and making it available in a single node profile.
See this manual's @seclink["profiles"]{section on profiles} for more
information about how profiles work.


@itemlist[#:style 'ordered

    @instructionstep["Start Experiment"]{
        @screenshot["powder-start-experiment-menu.png"]

        
        After logging in, you are taken to your main status
	@link["https://powderwireless.net/user-dashboard.php"]{dashboard}.
	Select ``Start Experiment'' from
	the ``Experiments'' menu.
    }

    @instructionstep["Select a profile"]{

        @screenshot["powder-srs-2.png"]

        The ``Start an Experiment'' page is where you will select a profile
        to instantiate. For this tutorial we will use the @bold{srsLTE-SIM}
	profile, which is the default profile on @(tb), so you should be able to simply
	select "Next" to proceed.
	
	 If it is not the default you may find the srsLTE-SIM profile as follows: Follow
        @link["https://www.powderwireless.net/p/PowderProfiles/srsLTE-SIM"]{this link}
        or click the ``Change Profile'' button, enter ``srsLTE-SIM'' in the search box,
	select ``srsLTE-SIM'' from the list on the left and click ``Select Profile''.

	@screenshot["powder-srs-1.png"]

        Once you have the correct profile selected, click ``Next''

        @screenshot["powder-srs-2.png"]
    }

    @instructionstep["Finalize"]{

        @screenshot["powder-srs-3.png"]

	More complex profiles will have a ``Parameterize'' step.
	For the profile used in this tutorial we jump directly to
	the ``Finalize'' step.

	Here you can optionally provide a name for your experiment.
	If you don't, @(tb) will make a name for you.

	In the ``Finalize'' step you need to select a ``Cluster''.
	For this profile, select ``Emulab'' from the dropdown menu.

	Click ``Next'' to proceed.
    }

    @instructionstep["Schedule"]{

        @screenshot["powder-srs-4.png"]

	In the ``Schedule'' step, go with the default ''Start immediately''
	and select ``Finish''. @(tb) will start provisioning the resources
	you requested. 
    }


    @instructionstep["Powder instantiates your profile"]{

        @(tb) will take a few minutes to bring up your experiment, as
        many things happen at this stage, including selecting suitable
        hardware, loading disk images on local storage, booting bare-metal
        machines, re-configuring the network topology, etc. While this is
        happening, you will see this status page:

        @screenshot["powder-srs-5.png"]
        
        @margin-note{Provisioning is done using the 
        @link["http://groups.geni.net/geni/wiki/GeniApi"]{GENI APIs}; it
        is possible for advanced users to bypass the @(tb) portal and
        call these provisioning APIs from their own code. A good way to
        do this is to use the @link["https://geni-lib.readthedocs.org"]{@tt{geni-lib} library for Python.}}


        As soon as a set of resources have been assigned to you, you will see
        details about them at the bottom of the page (though you will not be
        able to log in until they have gone through the process of imaging and
        booting.) 
    }

    @instructionstep["Your experiment is ready!"]{

    	 @screenshot["powder-srs-6.png"]

         When the web interface reports the state as ``Ready'', your experiment
         is provisioned, and you can proceed to the next section.

    }

]

@section{Exploring Your Experiment}

Now that your experiment is ready, take a few minutes to look at various parts
of the @(tb) status page to help you understand what resources you've got and what
you can do with them.

@subsection{Experiment Status}

The panel at the top of the page shows the status of your experiment---you can
see which profile it was launched with, when it will expire, etc. The
buttons in this area let you make a copy of the profile (so that you can
@seclink["creating-profiles"]{customize it}), ask to hold on to the resources
for longer, or release them immediately.

@screenshot["powder-srs-6.png"]

Note that the default lifetime for experiments on @(tb) is less than a day;
after this time, the resources will be reclaimed and their disk contents will
be lost. If you need to use them for longer, you can use the ``Extend'' button
and provide a description of why they are needed. Longer extensions require
higher levels of approval from @(tb) staff. You might also consider
@seclink["creating-profiles"]{creating a profile} of your own if you might need
to run a customized environment multiple times or want to share it with others.

You can click the title of the panel to expand or collapse it.


@subsection{Profile Instructions}

Profiles may contain written instructions for their use. Clicking on the title
of the ``Profile Instructions'' panel will expand (or collapse) it; in this
case, the instructions provide details on how to run the pre-installed
srsLTE eNodeB and UE tools

@screenshot["powder-srs-7.png"]

@subsection{Topology View}

At the bottom of the page, you can see the topology of your experiment. This
profile has a single compute node. The names given for nodes in an
experiment are the names assigned as part of the profile; this way, every time you instantiate
a profile, you can refer to the nodes using the same names, regardless of which
physical hardware was assigned to them. The green boxes around each node
indicate that they are up; click the ``Refresh Status'' button to initiate a
fresh check.

@screenshot["powder-srs-8.png"]

It is important to note that most nodes in @(tb) have at least @italic{two}
network interfaces: one ``control network'' that carries public IP
connectivity, and one ``experiment network'' that is isolated from the Internet
and all other experiments. It is the experiment net that is shown in this
topology view.  You will use the control network to @(ssh) into your nodes and
interact with them. This separation gives you more
freedom and control in the private experiment network, and sets up a clean
environment for @seclink["repeatable-research"]{repeatable research}.


@subsection[#:tag "powder-tutorial-list-view"]{List View}

The list view tab shows similar information to the topology view, but in a
different format. It shows the identities of the nodes you have been
assigned, and the full @(ssh) command lines to connect to them. In some
browsers (those that support the @tt{ssh://} URL scheme), you can click on the
SSH commands to automatically open a new session. On others, you may need to
cut and paste this command into a terminal window. Note that only public-key
authentication is supported, and you must have set up an @(ssh) keypair on your
account @bold{before} starting the experiment in order for authentication to
work.

@screenshot["powder-srs-9.png"]

@subsection{Manifest View}

The third default tab shows a
@link["http://groups.geni.net/geni/wiki/GENIExperimenter/RSpecs#ManifestRSpec"]{manifest}
detailing the hardware that has been assigned to you. This is the
@seclink["rspecs"]{``request'' RSpec} that is used to define the profile,
annotated with details of the hardware that was chosen to instantiate your
request. This information is available on the nodes themselves using the
@link["http://groups.geni.net/geni/wiki/GeniGet"]{@tt{geni-get}} command, 
enabling you to do rich scripting that is fully aware of both the requested
topology and assigned resources.

@margin-note{Most of the information displayed on the @(tb) status page comes
directly from this manifest; it is parsed and laid out in-browser.}

@screenshot["powder-srs-10.png"]

@subsection{Graphs View}

The final default tab shows a page of CPU load and network traffic
graphs for the nodes in your experiment.  On a freshly-created
experiment, it may take several minutes for the first data to appear.
After clicking on the ``Graphs'' tab the first time, a small reload icon
will appear on the tab, which you can click to refresh the data and
regenerate the graphs.  

@screenshot["powder-srs-11.png"]

@subsection[#:tag "powder-tutorial-actions"]{Actions}

In both the topology and list views, you have access to several actions that
you may take on individual nodes. In the topology view, click on the node to
access this menu; in the list view, it is accessed through the icon in the
``Actions'' column. Available actions include rebooting (power cycling) a node,
and re-loading it with a fresh copy of its disk image (destroying all data on
the node).  While nodes are in the process of rebooting or re-imaging, they
will turn yellow in the topology view. When they have completed, they will
become green again. The @seclink["powder-tutorial-web-shell"]{shell} action
is described in more detail below.

@screenshot["powder-srs-12.png"]

@subsection[#:tag "powder-tutorial-web-shell"]{Web-based Shell}

@(tb) provides a browser-based shell for logging into your nodes, which is
accessed through the action menu described above. While this shell is
functional, it is most suited to light, quick tasks; if you are going to do
serious work, on your nodes, we recommend using a standard terminal
and @(ssh) program.

This shell can be used even if you did not establish an @(ssh) keypair with
your account.

Two things of note:

@itemlist[
    @item{Your browser may require you to click in the shell window before
        it gets focus.}

    @item{Depending on your operating system and browser, cutting and pasting into
        the window may not work. If keyboard-based pasting does not
        work, try right-clicking to paste.}
]

@screenshot["powder-srs-13.png"]

@section[#:tag "powder-use-srslte"]{Using the srsLTE tools}

srsLTE provide software which, when paired with SDR hardware,
can realize fully functional UE and eNodeB functionality. In this
profile we will use more basic srsLTE provided tools to
``emulate'' UE and eNodeB interaction.

To "send" traffic from the eNodeB to an output file: 

@verbatim{/usr/local/srsLTE/build/lib/examples/pdsch_enodeb -o foo -n 5 -m 9 -v}

@screenshot["powder-srs-14.png"]

To "receive" traffic at the UE from the output file: 

@verbatim{/usr/local/srsLTE/build/lib/examples/pdsch_ue -i foo -n 5 -r 1234 -v -d}

@screenshot["powder-srs-15.png"]

You should see output related to the ``emulated'' eNodeB/UE interaction.

The node has both the srsGUI library (for real time display) and the srsLTE source
code installed. (See below.)

If you have ssh and X working on your setup, you should be able to run the
receive command without the -d option to get a ``real time'' display of
the signals ``received'' the UE. You would need to increase the
number sub-frames generated. E.g.:

@verbatim{/usr/local/srsLTE/build/lib/examples/pdsch_enodeb -o foo -n 10000 -m 9 -v}

@verbatim{/usr/local/srsLTE/build/lib/examples/pdsch_ue -i foo -n 10000 -r 1234 -v}


@section[#:tag "powder-srs-in-dig"]{Digging deeper}

Relevant pointers to dig deeper:

For real time plots compile and install the srsGUI library before building srsLTE: 

@link["https://github.com/srsLTE/srsGUI"]{https://github.com/srsLTE/srsGUI}

The srsLTE source with instructions on how to compile and run it:

@link["https://github.com/srsLTE/srsLTE"]{https://github.com/srsLTE/srsLTE}

The srsLTE tools used in the simple example above is described here: 

@link["https://git.networks.imdea.org/nicola_bui/imdeaowl/blob/92e902d00690bf58aa9c321dcd1e2566806656e6/README.md"]{IMDEA-OWL}

@section{Terminating the Experiment}

Resources that you hold in @(tb) are real, physical machines and are
therefore limited and in high demand. When you are done, you should release
them for use by other experimenters. Do this via the ``Terminate'' button on
the @(tb) experiment status page.

@screenshot["powder-srs-16.png"]

@bold{Note:} When you terminate an experiment, all data on the nodes is lost,
so make sure to copy off any data you may need before terminating.

If you were doing a real experiment, you might need to hold onto the nodes for
longer than the default expiration time. You would request more time by using
the ``Extend'' button the on the status page. You would need to provide a
written justification for holding onto your resources for a longer period of
time.

@section{Taking Next Steps}

Now that you've got a feel for for what @(tb) can do, there are several
things you might try next:

@itemlist[
    @item{@seclink["powder-tutorial"]{Do the @(tb) OAI Tutorial} to instantiate a slightly more
    complex profile.}
    @item{@seclink["create-project"]{Create a new project} to continue working
    on @(tb) past this tutorial}
    @item{Read more about the @seclink["basic-concepts"]{basic concepts} in
        @(tb)}
    @item{Try out different @seclink["hardware"]{hardware}}
    @item{Learn how to @seclink["creating-profiles"]{make your own profiles}}

]
