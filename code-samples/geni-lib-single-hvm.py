"""An example of constructing a profile with a single Xen VM in HVM mode.

Instructions:
Wait for the profile instance to start, and then log in to the VM via the
ssh port specified below.  (Note that in this case, you will need to access
the VM through a high port on the physical host, since we have not requested
a public IP address for the VM itself.)
"""

import geni.portal as portal
import geni.rspec.pg as rspec
# Import Emulab-specific extensions so we can set node attributes.
import geni.rspec.emulab as emulab

# Create a Request object to start building the RSpec.
request = portal.context.makeRequestRSpec()
 
# Create a XenVM
node = request.XenVM("node")

# Set the XEN_FORCE_HVM custom node attribute to 1 to enable HVM mode:
node.Attribute('XEN_FORCE_HVM','1')

# Print the RSpec to the enclosing page.
portal.context.printRequestRSpec()
