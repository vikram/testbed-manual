#lang scribble/manual
@(require "defs.rkt")

@title[#:tag "hardware" #:version apt-version]{Hardware}

@section[#:tag "powder-bs-hw"]{Base-station resources}

Each @(tb) base station site includes a collection of software defined
radio devices from National Instruments (NI) connected to Commscope
and Keysight antennas.  A handful of sites (one now, and three later)
include massive MIMO equipment from Skylark Wireless. Each device has
one or more dedicated 10G links to aggregation switches at the Fort
Douglas aggregation point.  These network connections can be flexibly
paired with compute at the aggregation point or slightly further
upstream with Emulab/CloudLab resources.

@itemlist[
  @item{Antennas (connectivity defined in SDR equipment section)
    @itemlist[
      @item{1 x 10-port Commscope VVSSP-360S-F multi-band
        @itemlist[
          @item{360 horizontal beamwidth, ~20 degree vertical}
          @item{8.2 dBi gain from 2.3 - 2.69 GHz}
          @item{4.9 dBi gain from 3.4 - 3.8 GHz}
          @item{4-port MIMO-capable Cellular elements (1695 - 2690 MHz)}
    	  @item{4-port MIMO-capable CBRS elements (3400 - 3800 MHz)}
    	  @item{2-port 5 GHz elements (5150 - 5925 MHz)}
          @item{@link["https://www.commscope.com/catalog/antennas/product_details.aspx?id=69448"]{Specifications Website}}
	]
      }

      @item{1 x 1-port broadband Keysight N6850A omnidirectional
        @itemlist[
	  @item{50 - 6000 MHz}
	]
      }
    ]
  }
    
  @item{NI SDR Equipment
    @itemlist[
      @item{Note: The current configuration is described below.  This
          is expected to change in the coming months as NI N310 units
          are brought online and RF front-end circuits are completed
          and deployed (ETA Sept. 2019).
      }
      @item{2 x USRP X310 with UBX160 daughtercards
        @itemlist[
          @item{Channel 'A' TX/RX and RX2 ports of device 1 through a frontend
                to a Cellular port of VVSSP-360S-F}
          @item{Channel 'A' TX/RX port of device 2 on a CBRS port of
                VVSSP-360S-F}
          @item{RF front-end for Band 7 cellular providing FDD, reduced noise
                figure and 4W maximum (saturated) power}
          @item{No add-on RF front-ends for gain/functionality yet in CBRS}
          @item{2 x 10 Gbe backhaul links}
	]
      }
      @item{Keysight (broadband) antenna currently unavailable.}
    ]
  }

  @item{Skylark Wireless Massive MIMO Equipment

    Skylark equipment consists of chains of multiple radios connected
    through a central hub.  This hub is 64x64 capable, and has 4 x 10
    Gbe backhaul connectivity.  @(tb) provides "big iron" compute
    (Dell d840 nodes, see compute section) for pairing with the
    massive MIMO equipment.

    @itemlist[
      @item{Note: Available at Merrill Engineering Building site}
      @item{32 x IRIS-030-D 2x2 transceiver radios
        @itemlist[
	  @item{Includes IRIS-FE-03-CBRS front-end modules}
	    @itemlist[
	      @item{BRS and CBRS capable: 2555 - 2655, 3550 - 3700 MHz}
	      @item{26 dBm, 2 x 2 TDD}
	    ]
    	  @item{Connected to dual-polarized antenna elements
	    @itemlist[
	      @item{100 degree beamwidth, 5.5 dBi}
	    ]
	  }
	]
      }
      @item{1 x FAROS-ENC-05-HUB aggregation hub
        @itemlist[
          @item{Provides power and connectivity to all IRIS SDRs}
	  @item{The 32 SDRs are connected in six chains to hub
	    @itemlist[
	      @item{4 chains have 4 Iris devices each}
      	      @item{2 chains have 8 Iris devices each (extended chains)}
      	      @item{13.2 Gbps connectivity across individual chains}
	    ]
	  }
	  @item{Trenz TE0808 SOM with XC7U9EG MPSoC}
	]
      }
    ]
  }

  @item{Upcoming changes:
    By Spring 2020, @(tb) base station sites will include 4 x NI SDR
    devices each; two USRP X310 SDRs (with UBX160 daughtercards), and
    two USRP N310 SDRs.  Custom, band/radio-specific RF front-ends
    will be included. Each SDR will be dedicated to a particular band:
    One N310 for 4 x 4 Cellular access (Commscope VVSSP-360S-F); One
    N310 for 4 x 4 CBRS access (VVSSP-360S-F); One X310 for 2x2 5 GHz
    band access (VVSSP-360S-F); One X310 for broadband access
    (Keysight N6850A). The custom RF front-ends will have PA
    and LNA components with 2W peak power, and both TDD and FDD paths
    available.  The particulars of these front-ends will
    be made available as the designs are completed and tested.
  }
]

@section[#:tag "powder-fe-hw"]{Fixed-endpoint resources}

Each Fixed Endpoint (FE) installation in POWDER contains an ensemble
of software defined radio (SDR) equipment from national Instruments
(NI) with complementary small form factor compute nodes.  There is no
wired backhaul, though the platform does provide seamless access via
cellular/WiFi to resources in an FE installation.  Endpoints are
mounted at human height level on the sides of buildings.

@itemlist[
  @item{Antennas
    @itemlist[
      @item{1 x Taoglas GSA.8841 wideband I-bar antenna
        @itemlist[
          @item{698 - 6000 MHz frequency range}
          @item{Approximately -2 dBi average gain across range}
          @item{@link["https://cdn2.taoglas.com/datasheets/GSA.8841.A.105111.pdf"]{Specifications Document}}
	]
      }
    ]
  }
  
  @item{NI SDR Equipment
    @itemlist[
      @item{Note: The current configuration is described below.  This
                  is expected to change in the coming months as RF
                  front-end circuits are completed and deployed.}

      @item{1 x NI USRP B210 SDR on nuc1
        @itemlist[
          @item{Channel 'A' RX2 port connected to dedicated GSA.8841 antenna}
          @item{Connected via USB 3.0 to NUC host (described below)}
          @item{@link["http://www.ettus.com/content/files/kb/b200-b210_spec_sheet.pdf"]{Specification Document}}
          @item{@link["http://files.ettus.com/manual/page_usrp_b200.html"]{Manual}}
	]
      }
      @item{1 x NI USRP B210 SDR on nuc2
        @itemlist[
          @item{Channel 'A' connected to Band 7 FDD frontend}
          @item{RF front-end provides FDD, reduced noise figure and 4W
                maximum (saturated) power}
          @item{Connected via USB 3.0 to NUC host (described below)}
          @item{@link["http://www.ettus.com/content/files/kb/b200-b210_spec_sheet.pdf"]{Specification Document}}
          @item{@link["http://files.ettus.com/manual/page_usrp_b200.html"]{Manual}}
	]
      }
    ]
  }

  @item{Intel NUC Compute
    @itemlist[
      @item{1 x Intel NUC8i7BEH small form factor PC
        @itemlist[
	  @item{Intel Core i7-8559U}
	  @item{32 GB RAM (2 x 16GB Corsair Vengeance 2400 MHz DDR4 SODIMM)}
    	  @item{250 GB NVMe storage (Kingston)}
    	  @item{@link["https://ark.intel.com/content/www/us/en/ark/products/126140/intel-nuc-kit-nuc8i7beh.html"]{Specification Document}}
	]
      }
    ]
  }
]

@section[#:tag "powder-iris-endpoints"]{Skylark IRIS Endpoints}

Skylark IRIS SDRs are deployed as endpoints in two eastern facing offices of
the Merrill Engineering Building. These are connected via 1 Gbps copper links.

@itemlist[
  @item{Skylark SDR Equipment
    @itemlist[
      @item{1 x IRIS-030-D
        @itemlist[
          @item{Includes IRIS-FE-03-CBRS front-end modules
            @itemlist[
              @item{BRS and CBRS capable: 2555 - 2655, 3550 - 3700 MHz}
              @item{26 dBm, 2 x 2 TDD}
            ]
          }
          @item{@link["https://www.skylarkwireless.com/products"]{Vendor Info}}
        ]
      }
    ]
  }
]

@section[#:tag "powder-ne-hw"]{Near-edge computing resources}

The following resources are connected to the Powder base-stations via
100Gb links in an aggregation point at the Fort Douglas datacenter.

@(nodetype "d740" 16 "Skylake, 24 cores"
    (list "CPU"   "2 x Xeon Gold 6126 processors (12 cores, 2.6Ghz)")
    (list "RAM"   "192GB Memory (12 x 16GB RDIMMs, 2.67MT/s)")
    (list "Disks" "2 x 240GB SATA 6Gbps SSD Drives")
    (list "NIC"   "10GbE Dual port embedded NIC (Intel X710)")
    (list "NIC"   "10GbE Dual port converged NIC (Intel X710)")
)

@(nodetype "d840" 3 "Skylake, 64 cores"
    (list "CPU"   "4 x Xeon Gold 6130 processors (16 cores, 2.1Ghz)")
    (list "RAM"   "768GB Memory (24 x 32GB RDIMMs, 2.67MT/s)")
    (list "Disks" "240GB SATA 6Gbps SSD Drive")
    (list "Disks" "4 x 1.6TB NVMe SSD Drive")
    (list "NIC"   "10GbE Dual port embedded NIC (Intel X710)")
    (list "NIC"   "40GbE Dual port converged NIC (Intel XL710)")
)

All nodes are connected to two networks:

@itemlist[
    @item{A 10 Gbps @italic{Ethernet} @bold{``control network''}---this network
        is used for remote access, experiment management, etc., and is
        connected to the public Internet. When you log in to nodes in your
        experiment using @code{ssh}, this is the network you are using.
        @italic{You should not use this network as part of the experiments you
        run in Powder.}
    }

    @item{A 10/40 Gbps @italic{Ethernet} @bold{``experimental network''}.
        Each @code{d740} node has two 10Gb interfaces, one connected to each
        of two Dell S5248F-ON datacenter switches.
        Each @code{d840} node has two 40Gb interfaces, both connected to the
        switch which also hosts the corresponding mMIMO base station
	connections.
    }
]

The two S5248F-ON switches are connected to a third S5248F-ON "aggregation"
switch via 2 x 100GbE links each. All three switches host 10Gb connections
from the eight roof-top base-stations. The aggregation switch also hosts
100Gb uplinks to the MEB and DDC datacenters that contain further Powder
resources and uplinks to Emulab and CloudLab.

@section[#:tag "powder-cl-hw"]{Cloud computing resources}

In addition,
@(tb) can allocate bare-metal computing resources on any one of several
federated clusters, including @link["https://www.cloudlab.us"]{CloudLab}
and @link["https://www.emulab.net"]{Emulab}.
