#lang scribble/manual

@(require "defs.rkt")

@title[#:tag "advanced-storage" #:style main-style]{Storage Mechanisms}

@section[#:tag "storage-basics"]{Overview of Storage Mechanisms}

@(tb) offers a convenient way to specify storage resources in a profile.
A @italic{blockstore}, more commonly known as a @italic{dataset}, is an
abstraction of a block-addressable storage container of a specified size.
Think of a dataset as a virtual disk that only your experiments can access.
The most common way to use a dataset's capacity is to make a filesystem
on it, though that is up to the user.

A dataset may either be @italic{local}, allocated on a node disk and
directly accessed by the OS, or @italic{remote}, located on a shared storage
server and accessible via the experiment network fabric.

Datasets may also be either @italic{ephemeral}, where the content lasts
only as long as its referencing experiment, or @italic{persistent}, where
the lifetime is independent of an experiment and can thus be used across
multiple successive experiments.
Note that local datasets are always ephemeral, since node disks are
re-imaged after every experiment use. (There is a pseudo-dataset type,
the @italic{image-backed dataset}, that can be used to explicitly capture
the contents of a local dataset in a reusable way).
Remote datasets can be ephemeral or persistent. 

Persistent datasets may be either @italic{short-term} or @italic{long-term}.
This is a policy distinction and not a technical one. The former is intended
to allow fixed-duration (e.g., one week) access to larger datasets with fewer
administrative hurdles. The latter is intended for longer term (e.g., months
to years) ongoing access to size-limited datasets. Long-term datasets remain
alive as long as they are being regularly used, but their creation is subject
to per-project quotas for total size and might require interaction with @(tb)
staff.

Persistent datasets may also be @italic{cloned}, giving individual nodes
their own mutable copy of a dataset. Currently, these clones are ephemeral,
with per-node changes lost at experiment termination. This limits their
utility. In the future, we plan to allow clones to be promoted to new
persistent datasets, and allowed to replace the dataset they are cloning.

Datasets are not the only form of storage available to users.
There is a legacy shared NFS filesystem that allows convenient but
limited concurrent sharing between nodes within and across experiments.
@powder-only{
@(tb), due to the not-always-connected nature of the control plane on
some endpoints, has a specialized @italic{write-back store} mechanism
that provided opportunistic off-loading of a limited amount of data
from those endpoints, even after an experiment terminates.
}

Important notes:
@itemlist[

@item{All local storage is ephemeral.
The contents of node disks will be lost when an experiment is terminated.
You are responsible for saving data from local disks.}

@item{Most of these storage mechanisms are intra-cluster only.
For example, a persistent dataset at Utah cannot be directly used by an
experiment running on Clemson nodes. A new dataset would have to be
created at Clemson, and the content explicitly copied over by the user from
the Utah dataset. Likewise, the shared NFS filesystem at one cluster is
independent of the shared filesystem at another cluster. Inter-cluster
sharing mechanisms, e.g., @tt{scp} or @tt{rsync}, must be provided
by the user at this time.
The one exception is the @seclink["ibdatasets"]{Image-backed Dataset}
described below, which can be moved between clusters.}

@item{All of these storage mechanisms are subject to failure.
@(tb) is not a storage provider. It is ultimately up to the users to make
sure that important data artifacts are preserved. Node disks in particular
are prone to failure; we do not configure them in any redundant way.
Infrastructure services such as the iSCSI storage servers and the NFS
servers have reasonable protection against hardware failures
(e.g., RAID or ZFS), but most clusters do not have off-site backup of
user data. If your data is important, @emph{back it up}!}

]

The following sections more concretely describe the storage resources
and workflows available to @(tb) users.

@section[#:tag "local-storage"]{Node-Local Storage}

All nodes in @(tb) have at least 100GB of local storage, in the form of one
or more SSDs (SATA or NVMe), spinning disks, or both.
See the @seclink["hardware"]{Hardware} section for details of the local
storage available on specific node types at each cluster.

By default, all @(tb) OS images have a 16GB system partition (partition 1)
on the boot disk. The OS installs typically take up 1-3GBs of this space,
leaving 10+GB of conveniently available storage (e.g., in @tt{/tmp}).
While this space is sufficient for many uses, there are times when more
is desired.

@subsection[#:tag "local-storage-dataset"]{Specifying Storage in a Profile -- Local Datasets}

If you know you will need additional storage before you create an experiment,
then you can specify the storage needs in your profile by configuring one or
more local datasets.
See the @seclink["storage-example-local-dataset"]{Local Dataset} example
below.
These datasets are automatically created by the @(tb) node configuration
scripts at experiment instantiation. If you specify a mountpoint, the system
will automatically create a filesystem in the dataset on first boot,
and mount it on every boot.

Local datasets are implemented on Linux using LVM. When one or more
datasets are required on a Linux node, the available storage on all
drives is combined into an LVM volume group and individual datasets are
allocated (striped across all involved disks) as logical volumes from that.

At this time, there is no way to specify redundancy on a local dataset.
The LVM volume group and its corresponding logical volumes are effectively
RAID0.

@subsection[#:tag "local-storage-hacks"]{Allocating Storage in a Running Experiment}

If you are in a situation where an existing experiment needs more space
(i.e., you did not specify a dataset in the profile), we provide a script
that can be run to quickly create a new filesystem using the remaining space
on the system (boot) disk:

@codeblock{
    sudo /usr/local/etc/emulab/mkextrafs.pl /mydata
}

This will provide you with anywhere from 90GB to 1TB, depending on the
node type you are on.

It is also possible to use Linux tools to create your own partitions,
filesystems, LVM, ZFS, RAID, or other storage configurations. You can
also resize the OS partition to include all storage on the boot disk.
These techniques are @emph{strongly discouraged}, as the resulting
configurations will only last til the experiment ends and may interfere
with the @(tb) imaging tools.

@subsection[#:tag "local-storage-persist"]{Persisting Local Data}

Since most @(tb) nodes are directly accessible from the Internet, you
can use your favorite tools (e.g., @tt{tar}/@tt{scp} or @tt{rsync})
to offload your data from a node to your home before the experiment,
terminates. You can copy data from
local disks to the @seclink["shared-storage"]{shared NFS filesystem},
but this practice is @emph{strongly discouraged}.

You can also make your data part of a custom OS image. By placing it in
a directory like @tt{/data}, it will be included in any snapshot you make.
Do @emph{not} put it in your home directory on the node, or transient
locations like @tt{/tmp} or @tt{/var/tmp} as those are not captured in
snapshots. This practice of ``baking'' data into an OS image is generally
not a good idea as it ties the data to a specific OS and can result in
very large images which might put you over disk quota.

@(tb) does provide one way to persist local disk data in a format that
can be loaded on future experiment nodes independent of the OS image used
and can be exported to other clusters.
These @seclink["ibdatasets"]{Image-backed Datasets} are described next.

@section[#:tag "ibdatasets"]{Image-backed Datasets}

An image-backed dataset is a snapshot of a local dataset created using
a @(tb) @seclink["disk-images"]{Disk Image}. Since disk images can be used
across clusters, an image-backed dataset is a convenient way of both
persisting data from a node and enabling it to be used in different
experiments on different clusters.

An image-backed dataset can be updated from any node on which it is installed
by taking a @italic{snapshot} as you would an OS image. However, image-backed
datasets are not versioned, there is always only one version of the dataset
across all clusters. Note also that creating a new version of the dataset
does not affect other currently installed copies of the dataset on other nodes.
While the portal will not allow simultaneous snapshots of the same dataset,
it is ultimately up to the user to ensure consistency across uses of the
dataset.

An image-backed dataset can be protected as readable (install-able) by
just members of your project or by anyone. Independently, they can be
protected as writable (update-able) either by just yourself or by members
of your project.

Examples of creation, use and updating of an image-backed dataset are
shown in the @seclink["storage-examples"]{Storage Examples} section.

Note that the size of image-backed datasets is limited by individual clusters.
Typically, this limit is around 20GB of compressed data per image, so these
datasets are @emph{not} well suited to very large datasets.

Note also that image-backed datasets, like all @(tb) images, are in the
custom @italic{frisbee} format and cannot be easily examined or used
outside of @(tb).

@section[#:tag "remote-storage"]{Remote Datasets}

Remote datasets are network accessible storage volumes. Specifically, they
are hosted on per-cluster, infrastructure-provided storage servers and exposed
to experiment nodes via the iSCSI protocol on the experiment network fabric.
Most, but not all, cluster in @(tb) have at least one storage server.
See the @seclink["hardware"]{Hardware} section for details on available
remote storage at each cluster. A remote dataset appears on an experiment
node as disk device, typically @tt{/dev/sdb} or @tt{/dev/sdc}, depending on
how many local disks a node has. Multiple datasets will result in multiple
disk devices.

Remote datasets are intended to provide access to a larger quantity of
storage than what is available locally on most nodes. While the total space
available on @(tb) storage servers is modest (20-100TB), it does at least
allow for multi-TB datasets on nodes. Because these datasets are accessed
as part of an experiment's private network topology, the content is more
secure and there is less impact on other experiments relative to sharing
mechanisms that use the control network
(e.g., the @seclink["shared-storage"]{shared NFS filesystem}).

An ephemeral remote dataset allows node-private storage larger than what
is available on the local disks. They are created at experiment instantiation
and destroyed at experiment termination.

Persistent remote datasets provide efficient access to remote storage that
persists across experiment instantiations. Because the data resides remotely
and does not need to be copied in at experiment startup and copied off at
termination, it potentially allows large-data experiment instances to run
for shorter lengths of time per instantiation. Simultaneous access to
remote datasets by multiple nodes within and across experiments is also
possible, @emph{with certain limitations}. Read-only sharing of a dataset,
or use of per-node read-write @italic{clones} of a dataset, are always safe.
Simultaneous read-write sharing of a remote dataset is possible, but almost
certainly not what you want. Unless the OSes on all sharing nodes are
coordinating their writes to the dataset, you will almost certainly wind up
with a corrupted dataset. If you need a shared filesystem on persistent
storage, see the example @seclink["storage-example-remote-nfs"]{Shared Filesystem}
profile.

@section[#:tag "shared-storage"]{NFS Shared Filesystems}

The original Emulab mechanism for sharing and persistence was a set of
shared NFS filesystems hosted by an infrastructure server and accessed
over the control network.
@not-elab{
This mechanism is still available in @(tb), but only for the @tt{/proj}
hierarchy.
}
While NFS provides an extremely easy to understand and use method for
sharing and persisting data, it is extremely inefficient for some workloads
such as those that are metadata intensive (e.g., creating lots of files when
unpacking a tarball) or bandwidth intensive (e.g., simultaneous reading
or writing of large files). This can place considerable load on a central
shared resource and adversely affect other experiments and even the @(tb)
control framework. For this reason, NFS is @emph{strongly discouraged}
for real-time data capture or logging.

@powder-only{
@section[#:tag "wbstore"]{Write-back Storage for Fixed and Mobile Endpoints}

In @(tb), fixed and mobile endpoints are themselves clusters (aggregates)
with their own infrastructure and nodes. However, these clusters have the
unique property that their Internet-facing network is a wireless rather
than wired connection and will operate at lower bandwidth. Mobile endpoints,
currently deployed on campus shuttle buses, have an additional restriction
that the entire cluster may be unavailable for extended periods (hours to weeks)
when the bus is out of service and turned off.

In this environment, experiments will be using nodes and producing data
that they will want to offload, but may be unable to do so before the
termination of their experiment, due to the lower bandwidth and possible
sporadic connectivity. Extending experiments just to allow users to offload
their data is not viable as it ties up scare resources and also places the
burden of tracking when an endpoint is up on the experiment user.

To address this, @(tb) added a mechanism where users can write the data
persistently on an endpoint and the system will opportunistically offload
("write back") data to the central @(tb) cluster whenever a endpoint
is running. This offload may continue even after an experiment has
terminated. Once all data from all endpoints involved in an experiment
has been written back, the @(tb) cluster will create a tarball and inform
the experiment creator via email. That user can then download the tarball
and access the data.

To use the @italic{write-back store} on an endpoint node, the user just
needs to place the data they want written back into a special directory,
@tt{/var/emulab/save}, which is a symlink to a unique location on the
endpoint's persistent @tt{/proj} filesystem. The content of this
directory, including subdirectories, is mirrored to a corresponding
directory on the central @(tb) cluster using the @tt{syncthing} tool.
There is a quota on the special directory that ensures that users do not
write excessive quantities of data that would take an unreasonable amount
of time to offload given the upload speed and general availability of the
endpoint. Note that an important consequence of quotas and mirroring is
that removing files from the special directory to stay under quota, will
result in the files being removed in the write-back copy as well.

Write-back is a continuous process and performed simultaneously from all
endpoints involved in an experiment. The data from all endpoints can be
viewed as it is uploaded if the user has a node allocated on the central
@(tb) cluster. However, the data collection should not be considered
real-time or even consistent at any given point in time, due to the
rate-limiting of the endpoint collector process and the way in which
that collector performs updates. The only complete picture of the data
is the post-experiment tarball that is made available for download.
Note also that no guarantee is provided for how long it might take after
experiment termination before this tarball is available.
}

@section[#:tag "storage-summary"]{Storage Type Summary (TL;DR)}

The following table attempts to summary the various storage mechanisms and
their attributes. @tt{Persistent} indicates whether modified data remains after
an experiment is terminated. @tt{Multi-node} indicates if and how the data
can be used by multiple nodes simultaneously, @tt{Capacity} is the rough
size of the storage available for an instance of the mechanism, @tt{Throughput}
is a rough estimate of the best-case (sequential) throughput of the storage
mechanism, and @tt{Use Cases} describes when the mechanism is appropriate
and notes other characteristics.

@(tabular #:style 'boxed #:sep (hspace 2) #:row-properties '((top bottom-border)) (list
    (list @bold{Method} @bold{Persistent} @bold{Multi-node} @bold{Capacity} @bold{Throughput} @bold{Use Cases})
    (list "Per-node local root filesystem"
          "No"
          "No"
	  "~10GB"
	  "100-300MB/sec"
	  "Sufficient for the majority of experiments; no explicit setup required")
    (list "Per-node extra filesystem"
    	  "No"
    	  "No"
	  "90GB to 1TB"
	  "100-200MB/sec"	  
	  "When more than 10GB is needed; setup after experiment start; user must choose an appropriate node type; user must explicitly create")
    (list "Per-node local blockstore"
    	  "No"
    	  "No"
	  "90GB to 40TB"
	  "100-1000MB/sec"	  
	  "When up to 40TB is needed; specified by size in profile, system picks the node type; automatically setup; may stripe on multiple disks")
    (list "Image-backed dataset"
    	  "Yes"
    	  "Yes, all nodes get a copy"
	  "10-20GB"
	  "100-1000MB/sec"	  
	  "When persistence and local disk speed is needed, but not large capacity; must be snapshotted to save modifications")
    (list "Infrastruture-provided shared NFS filesystem"
    	  "Yes"
    	  "Yes, all nodes read-write share"
	  "up to 100GB"
	  "20-100MB/sec"
	  "When true sharing is needed, use is discouraged for many-node or high IO-op experiments")
    (list "Ephemeral remote datasets"
    	  "No"
    	  "No"
	  "up to 10TB"
	  "50-200MB/sec"
	  "When large capacity but not high throughput is needed on a single node")
    (list "Persistent remote datasets"
    	  "Yes"
    	  "Yes, all nodes can read-write share"
	  "up to 10TB"
	  "50-200MB/sec"
	  "When persistence and large capacity but not high throughput are needed; read-write sharing between nodes requires @emph{extreme care}.")
    (list "Persistent remote dataset clones"
    	  "No"
    	  "Yes, all nodes get a copy"
	  "up to 10TB"
	  "50-200MB/sec"
	  "When large scale sharing and large capacity but not persistence or high throughput are needed; clones are read-write but changes are not persistent")
))

@;{
Pros and cons:

local disk
 * pro: easiest and most natural to use
 * pro: offers generally fast and consistent performance...
 * con: ...but only within a particular node type
 * con: always ephemeral, hence takes time to onboard/offload data
 * pro: accessible only on the local node in a multi-node experiment,
        limiting side-effects and data visibility
 * con: accessible only on the local node in a multi-node experiment,
        limiting sharing and coordination of nodes

image-backed dataset
 * pro: provides a mechanism to onboard/offload data to a local dataset
 * pro: efficient if the same data is needed on multiple nodes due to imaging system
 * con: size-limited due to current limitations of the imaging system

remote ephemeral dataset
 * pro: potentially much larger that the storage available on a single node
 * con: generally lower throughput than local disks
 * con: can be mapped only to a single node, hence not usable as a
        sharing/coordination mechanism.

remote persistent dataset
 * pro: potentially much larger that the storage available on a single node
 * con: generally lower throughput than local disks

NFS shared filesystem
 - cite the NFS profile as a work-around for a shared, persistent FS

wbstores (Powder only)
}

@section[#:tag "storage-examples"]{Example Storage Profiles}

@subsection[#:tag "storage-example-local-dataset"]{Creating a Node-local Dataset}

If you know in advance that you will need more that the ~10GB available
on the root filesystem of any OS image, you can create a local dataset
with a specified size as demonstrated in this profile:

@profile-code-sample["PortalProfiles" "local-diskspace"]

Instantiating this profile will give you a single node with a dataset
containing an empty filesystem mounted at @tt{/mydata}.

@subsection[#:tag "storage-example-imdataset"]{Creating an Image-backed Dataset from a Node-local Dataset}

If you have created a node-local dataset as described above and populated it,
then you can persist it by creating a new image-backed dataset. Click on the
@tt{"Create Dataset"} option in the @tt{Storage} menu. This will bring up the
form to create a new dataset:

@screenshot["create-imdataset.png"]

As shown, choose a name for your dataset and optionally the project the
dataset should be associated with. Be sure to select @bold{Image Backed} for
the type. Then choose the experiment (@tt{Instance}), which node in the
experiment (@tt{Node}), and which local dataset on the node (@tt{BS Name}).
The dataset name will be the first argument to the @tt{node.Blockstore}
method invocation in the profile used to create the local dataset (@tt{bs}
in the example above).

Before clicking the @tt{Create} button, make sure that you have no processes
running on the node and accessing the mounted filesystem. This might include
processes logging to a file in that filesystem or your interactive shell if
you are @tt{cd}'ed to that directory. If you do not do this, the image
creation will fail when it tries to unmount the filesystem to ensure a
consistent snapshot.

After clicking @tt{Create}, the process can take several minutes or longer,
depending on the size of the file system. Progress will be displayed on
the page:

@screenshot["snapshot-dataset.png"]

When the progress bar reaches the @tt{Ready} stage, your new dataset is
ready! It will now show up in your @tt{Storage} drop-down under
@tt{My Datasets} and can be used in new experiments.

@subsection[#:tag "storage-example-update-imdataset"]{Using and Updating an Image-backed Dataset}

To use an existing image-backed dataset, you will need to reference it in
your profile, as demonstrated in:

@profile-code-sample["PortalProfiles" "imdataset"]

This profile takes the dataset @italic{URN} to use as a parameter during
instantiation. You can find the URN for your dataset on the information
page for the dataset. From the @tt{Storage} drop-down, click on
@tt{My Datasets}, find the name of your dataset in the list, and click
on it.

Once instantiated, the dataset will be accessible under @tt{/mydata}
(or whatever mountpoint you specified).

If you make changes and want to preserve them, you can update your dataset
by using the @tt{Modify} button on the @tt{My Datasets} page for the
dataset in question.

@subsection[#:tag "storage-example-remote"]{Creating a Remote Dataset}

Creating a remote dataset is very similar to the process just described
for creating an image-backed dataset. Click on the @tt{"Create Dataset"}
option in the @tt{Storage} drop-down menu. This will bring up the form to
create a new dataset:

@bold{Screenshot TBD}

Fill in the fields:
@itemlist[
@item{Choose a name for your dataset and optionally the project the
dataset should be associated with.}

@item{Select @tt{Short term} or @tt{Long term} as the @tt{Type} depending
on your needs (click on ``Which type should I pick'' for more info).}

@item{Pick a @tt{Size}, keeping in mind that sizes in excess of 1TB are
likely to require administrative approval.}

@item{Pick the @tt{Cluster} at which the dataset will be created. Recall
that remote datasets are specific to a cluster and can only be used by
nodes at that cluster.}

@item{If you selected a short-term dataset, you will need to fill in
the expiration date (@tt{Expires}), again keeping in mind that dates more
than 1-2 weeks in the future will require administrative approval.}

@item{Pick the initial filesystem type you would like created on the dataset.
Almost certainly you will want to use the default @tt{ext4}. It is not
necessary to create a filesystem (choose ``none'') if you want to use the
dataset as a raw disk or if you want to create your own filesystem on it
when you first use it. Note however, if you do not choose a filesystem now,
then you cannot set a mountpoint when you first use the dataset.}

@item{Finally, select the read and write permissions for the dataset.}
]

After clicking @tt{Create}, you may get a message informing you
@tt{Your dataset needs to be approved!} and giving you a reason why.
If this is the case, your dataset will be shown as ``unapproved'' and
you can either let it go and see if it is approved by @(tb) administrators,
or you can @tt{Delete} and try again with different parameters.
(Note that @tt{Modify} will only allow you to change the permission settings
and cannot be used to alter the size or duration of the dataset.)
The creation process can take several minutes or longer, depending on
whether you specified a filesystem and what its size and type are.

Once it shows up in your @tt{My Datasets} list as @tt{valid}, you can
use it in new experiments.

@subsection[#:tag "storage-example-remote-single"]{Using a Remote Dataset on a Single Node}

Once you have created a remote dataset, you can make use of it in experiments.
In many situations, you may only need to use the dataset on a single node.
This is certainly the case after you have just created the dataset and need
to populate it. This following profile demonstrates how to use a remote
dataset:

@profile-code-sample["PortalProfiles" "longterm-dataset"]

You can find the URN for your dataset on the information page for the dataset.
From the @tt{Storage} drop-down, click on @tt{My Datasets}, find the name of
your dataset in the list, and click on it.

Note the ``fslink'' settings @tt{best_effort} and @tt{vlan_tagging}.
These should always be set since some node types have only a single
experimental interface. Since the remote dataset uses a network link
and your experiment topology might also include a LAN with multiple nodes
(see the following examples for multiple nodes), both uses will need to
share the physical interface.

@subsection[#:tag "storage-example-remote-nfs"]{Using a Remote Dataset on Multiple Nodes via a Shared Filesystem}

You cannot simply create a filesystem in a persistent remote dataset and
directly share that among nodes in an experiment. If you want to share a
standard Linux filesystem among nodes in an experiment, you can instead
have one node in your experiment map the dataset read-write and have it
act as an NFS server, exporting the dataset filesystem to all other nodes
in the experiment via NFS on a shared LAN. This profile configures such
an experiment with a variable number of client nodes:

@code-sample["geni-lib-nfs-dataset.py"]

@subsection[#:tag "storage-example-remote-clones"]{Using a Remote Dataset on Multiple Nodes via Clones}

TBD.
